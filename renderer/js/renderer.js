

const form = document.querySelector('#img-form');
const img = document.querySelector('#img');
const outputPath = document.querySelector('#output-path');
const filename = document.querySelector('#filename');
const heightInput = document.querySelector('#height');
const widthInput = document.querySelector('#width');

function loadImage(e){
    const file = e.target.files[0];

    if(!isImage(file)){
        alertError('Please Select an Image File');
        return;
    }

    alertSuccess('Image Loaded Successfully');
    // Get image dimensions
    const image  = new Image();
    image.src = URL.createObjectURL(file);
    image.onload = function(){
        heightInput.value = this.height;
        widthInput.value = this.width;
    };

    form.style.display = 'block';
    filename.textContent = file.name;
    outputPath.value = path.join(os.homedir(), 'ImageResizer');
}

function isImage(file){
    const acceptedImageTypes = ['image/jpeg', 'image/png', 'image/gif'];
    return file && acceptedImageTypes.includes(file.type);
}

function alertError(message){
    Toastify({
        text: message,
        duration: 3000,
        gravity: 'top',
        position: 'right',
        backgroundColor: 'linear-gradient(to right, #ff5f6d, #ffc371)',
        stopOnFocus: true
    }).showToast();
}

function alertSuccess(message){
    Toastify({
        text: message,
        duration: 3000,
        gravity: 'top',
        position: 'right',
        backgroundColor: 'linear-gradient(to right, #00b09b, #96c93d)',
        stopOnFocus: true
    }).showToast(); 
}

img.addEventListener('change', loadImage);