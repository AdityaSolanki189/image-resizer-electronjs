const path = require('path');
const {app, BrowserWindow, Menu} = require('electron');

// Create Window
function createWindow() {
    // Create the browser window.
    let win = new BrowserWindow({
        title: 'Image Resizer',
        width: 800,
        height: 600,
        webPreferences: {
            contextIsolation: true,
            nodeIntegration: true,
            preload: path.join(__dirname, './preload.js')
        }
    })

    // and load the index.html of the app.
    win.loadFile(path.join(__dirname, './renderer/index.html'));
}

// About Window
function aboutWindow(){
    let win = new BrowserWindow({
        title: 'About',
        width: 400,
        height: 300,
        webPreferences: {
            nodeIntegration: true
        }
    })

    win.loadFile(path.join(__dirname, './renderer/about.html'));
}

// App Ready
app.whenReady().then(() => {
    createWindow()

    const mainMenu = Menu.buildFromTemplate(menu);
    Menu.setApplicationMenu(mainMenu);

    app.on('activate', function () {
        if (BrowserWindow.getAllWindows().length === 0) 
            createWindow()
    });
});

// Menu Template
const menu = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Quit',
                click: () => {
                    app.quit();
                },
                accelerator: process.platform === 'darwin' ? 'Command+Q' : 'Ctrl+Q'
            },
            {
                label: 'About',
                click: () => {
                    aboutWindow();
                },
                accelerator: process.platform === 'darwin' ? 'Command+A' : 'Ctrl+A'
            }
        ]
    }
]

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') 
        app.quit()
}); 